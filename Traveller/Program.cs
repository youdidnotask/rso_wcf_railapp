﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Traveller.RailServiceReference;

namespace Traveller {

    class Client {
        RailServiceClient client = null;
        string startPoint, endPoint;
        DateTime startDateTime, endDateTime;

        public Client() {
            client = new RailServiceClient();
        }

        ~ Client() {
            if(client != null) {
                client.Close();
            }
        }

        private static bool ValidateLocation(string location) {
            return Regex.IsMatch(location, @"^[a-zA-Z]+$");
        }

        private bool EnterLocations() {
            Console.Write("START:\t");  startPoint = Console.ReadLine();
            Console.Write("END:\t");    endPoint = Console.ReadLine();

            return ValidateLocation(startPoint) && ValidateLocation(endPoint);
        }

        private bool EnterTime() {
            String start, end;
            startDateTime = new DateTime();
            endDateTime = new DateTime();
            Console.WriteLine("[YYYY-MM-DD HH:MM]");
            Console.Write("START:\t");  start = Console.ReadLine();
            Console.Write("END:\t");    end = Console.ReadLine();

            try {
                startDateTime = DateTime.Parse(start);
                endDateTime = DateTime.Parse(end);
                return true;
            }
            catch (FormatException) {
                return false;
            }
        }

        public void GetLocationsRequest() {
            Connection[] connections = client.GetConnections();
            Console.WriteLine("\n###############################################################\n");
            foreach (Connection c in connections) {
                Console.WriteLine("{0} {1}\t{2} {3}", c.StartPoint, c.StartDateTime, c.EndPoint, c.EndDateTime);
            }
            Console.WriteLine("\n###############################################################\n");
        }

        public void FindConnectionsRequest() {

            Connection[][] indirectConn;
            if(EnterLocations()) {
                indirectConn = client.FindConnections(startPoint, endPoint);
                Console.WriteLine("\n###############################################################\n");
                foreach (Connection[] list in indirectConn) {
                    foreach (Connection conn in list) {
                        Console.WriteLine("{0} {1} -> {2} {3}", conn.StartPoint, conn.StartDateTime, conn.EndPoint, conn.EndDateTime);
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("###############################################################\n");
            }
            else {
                Console.WriteLine("\nInvalid parameters!\n");
            }
        }

        public void FindConnectionsInTimeRequest() {
            Connection[][] indirectConn;
            if (EnterLocations() && EnterTime()) {
                indirectConn = client.FindConnectionsInTime(startPoint, startDateTime, endPoint, endDateTime);
                foreach (Connection[] list in indirectConn) {
                    foreach (Connection conn in list) {
                        Console.WriteLine("{0} {1} -> {2} {3}", conn.StartPoint, conn.StartDateTime, conn.EndPoint, conn.EndDateTime);
                    }
                    Console.WriteLine("");
                }
            }
            else {
                Console.WriteLine("\nInvalid parameters!\n");
            }
        }
    }

    class Program {
      
        private static int Menu() {
            Console.WriteLine("[1]\tList of available connections.");
            Console.WriteLine("[2]\tFind connection.");
            Console.WriteLine("[3]\tFind connection (in a specified period of time).");
            Console.WriteLine("[4]\tQuit.");
            Console.Write("select:\t");

            return Int32.Parse(Console.ReadLine());
        }

        static void Main(string[] args) {
            int selected = 0;
            try {
                Client client = new Client();

                while (selected != 4) {
                    try {
                        selected = Menu();
                        switch (selected) {
                            case 1:
                                client.GetLocationsRequest();
                                break;
                            case 2:
                                client.FindConnectionsRequest();
                                break;
                            case 3:
                                client.FindConnectionsInTimeRequest();
                                break;
                            case 4:
                                break;
                            default:
                                Console.WriteLine("Unknown command");
                                break;
                        }
                    }
                    catch (FaultException<InvalidArgumentException> ex) {
                        Console.WriteLine("\n\tERROR: {0}\n", ex.Detail.Message);
                    } catch(FaultException<LocationNotFoundException> ex) {
                        Console.WriteLine("\n\tERROR: {0}\n", ex.Detail.Message);
                    } catch (FaultException<ConnectionDoesNotExistException> ex) {
                        Console.WriteLine("\n\tERROR: {0}\n", ex.Detail.Message);
                    }
                }
                
            } catch(FaultException<NoConnectionsException> ex) {
                Console.WriteLine("\n\tERROR: {0}\n", ex.Detail.Message);
            } catch(EndpointNotFoundException ex) {
                Console.WriteLine(ex.Message);
            } 

            Console.ReadKey();
        }
    }
}
