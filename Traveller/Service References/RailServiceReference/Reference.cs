﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Traveller.RailServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Connection", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
    [System.SerializableAttribute()]
    public partial class Connection : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime EndDateTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EndPointField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime StartDateTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StartPointField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime EndDateTime {
            get {
                return this.EndDateTimeField;
            }
            set {
                if ((this.EndDateTimeField.Equals(value) != true)) {
                    this.EndDateTimeField = value;
                    this.RaisePropertyChanged("EndDateTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndPoint {
            get {
                return this.EndPointField;
            }
            set {
                if ((object.ReferenceEquals(this.EndPointField, value) != true)) {
                    this.EndPointField = value;
                    this.RaisePropertyChanged("EndPoint");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime StartDateTime {
            get {
                return this.StartDateTimeField;
            }
            set {
                if ((this.StartDateTimeField.Equals(value) != true)) {
                    this.StartDateTimeField = value;
                    this.RaisePropertyChanged("StartDateTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartPoint {
            get {
                return this.StartPointField;
            }
            set {
                if ((object.ReferenceEquals(this.StartPointField, value) != true)) {
                    this.StartPointField = value;
                    this.RaisePropertyChanged("StartPoint");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NoConnectionsException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
    [System.SerializableAttribute()]
    public partial class NoConnectionsException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidArgumentException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
    [System.SerializableAttribute()]
    public partial class InvalidArgumentException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="LocationNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
    [System.SerializableAttribute()]
    public partial class LocationNotFoundException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ConnectionDoesNotExistException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
    [System.SerializableAttribute()]
    public partial class ConnectionDoesNotExistException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="RailServiceReference.IRailService")]
    public interface IRailService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/GetConnections", ReplyAction="http://tempuri.org/IRailService/GetConnectionsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.NoConnectionsException), Action="http://tempuri.org/IRailService/GetConnectionsNoConnectionsExceptionFault", Name="NoConnectionsException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        Traveller.RailServiceReference.Connection[] GetConnections();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/GetConnections", ReplyAction="http://tempuri.org/IRailService/GetConnectionsResponse")]
        System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[]> GetConnectionsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/FindConnections", ReplyAction="http://tempuri.org/IRailService/FindConnectionsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.InvalidArgumentException), Action="http://tempuri.org/IRailService/FindConnectionsInvalidArgumentExceptionFault", Name="InvalidArgumentException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.LocationNotFoundException), Action="http://tempuri.org/IRailService/FindConnectionsLocationNotFoundExceptionFault", Name="LocationNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.ConnectionDoesNotExistException), Action="http://tempuri.org/IRailService/FindConnectionsConnectionDoesNotExistExceptionFau" +
            "lt", Name="ConnectionDoesNotExistException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        Traveller.RailServiceReference.Connection[][] FindConnections(string startPoint, string endPoint);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/FindConnections", ReplyAction="http://tempuri.org/IRailService/FindConnectionsResponse")]
        System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[][]> FindConnectionsAsync(string startPoint, string endPoint);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/FindConnectionsInTime", ReplyAction="http://tempuri.org/IRailService/FindConnectionsInTimeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.InvalidArgumentException), Action="http://tempuri.org/IRailService/FindConnectionsInTimeInvalidArgumentExceptionFaul" +
            "t", Name="InvalidArgumentException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.LocationNotFoundException), Action="http://tempuri.org/IRailService/FindConnectionsInTimeLocationNotFoundExceptionFau" +
            "lt", Name="LocationNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        [System.ServiceModel.FaultContractAttribute(typeof(Traveller.RailServiceReference.ConnectionDoesNotExistException), Action="http://tempuri.org/IRailService/FindConnectionsInTimeConnectionDoesNotExistExcept" +
            "ionFault", Name="ConnectionDoesNotExistException", Namespace="http://schemas.datacontract.org/2004/07/RailApp")]
        Traveller.RailServiceReference.Connection[][] FindConnectionsInTime(string startPoint, System.DateTime startDateTime, string endPoint, System.DateTime endDateTime);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRailService/FindConnectionsInTime", ReplyAction="http://tempuri.org/IRailService/FindConnectionsInTimeResponse")]
        System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[][]> FindConnectionsInTimeAsync(string startPoint, System.DateTime startDateTime, string endPoint, System.DateTime endDateTime);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IRailServiceChannel : Traveller.RailServiceReference.IRailService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RailServiceClient : System.ServiceModel.ClientBase<Traveller.RailServiceReference.IRailService>, Traveller.RailServiceReference.IRailService {
        
        public RailServiceClient() {
        }
        
        public RailServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RailServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RailServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RailServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Traveller.RailServiceReference.Connection[] GetConnections() {
            return base.Channel.GetConnections();
        }
        
        public System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[]> GetConnectionsAsync() {
            return base.Channel.GetConnectionsAsync();
        }
        
        public Traveller.RailServiceReference.Connection[][] FindConnections(string startPoint, string endPoint) {
            return base.Channel.FindConnections(startPoint, endPoint);
        }
        
        public System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[][]> FindConnectionsAsync(string startPoint, string endPoint) {
            return base.Channel.FindConnectionsAsync(startPoint, endPoint);
        }
        
        public Traveller.RailServiceReference.Connection[][] FindConnectionsInTime(string startPoint, System.DateTime startDateTime, string endPoint, System.DateTime endDateTime) {
            return base.Channel.FindConnectionsInTime(startPoint, startDateTime, endPoint, endDateTime);
        }
        
        public System.Threading.Tasks.Task<Traveller.RailServiceReference.Connection[][]> FindConnectionsInTimeAsync(string startPoint, System.DateTime startDateTime, string endPoint, System.DateTime endDateTime) {
            return base.Channel.FindConnectionsInTimeAsync(startPoint, startDateTime, endPoint, endDateTime);
        }
    }
}
