﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;

namespace RailApp {

    public class RailService : IRailService {
        private static readonly string filename = @"trains.csv";
        private List<Connection> connections = new List<Connection>();
        private List<List<Connection>> indirectConn;
        private List<string> locations;

        public RailService() {
            LoadData();
        }

        private void LoadData() {
            using (TextFieldParser parser = new TextFieldParser(filename)) {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                while(!parser.EndOfData) {
                    string[] fields = parser.ReadFields();
                    connections.Add(new Connection(
                        fields[0],
                        DateTime.Parse(fields[1]),
                        fields[2],
                        DateTime.Parse(fields[3]),
                        false
                    ));
                }                    
            }

            locations = new List<string>();
            foreach (Connection c in connections) {
                if (!locations.Contains(c.StartPoint)) {
                    locations.Add(c.StartPoint);
                }
                if (!locations.Contains(c.EndPoint)) {
                    locations.Add(c.EndPoint);
                }
            }
        }

        private bool ValidateLocation(string location) {
            return Regex.IsMatch(location, @"^[a-zA-Z]+$");
        }

        private bool ValidateDate(DateTime startDateTime, DateTime endDateTime) {
            return startDateTime < endDateTime;
        }

        private bool IsAvailable(string location) {
            return locations.Contains(location);
        }

        private void FindAll(List<Connection> availableConn, List<Connection> pastConn, string startPoint, string endPoint) {
            List<Connection> newPastConn;

            foreach (Connection conn in availableConn) {
                if (conn.StartPoint == startPoint && !conn.Visited) {
                    conn.Visited = true;

                    if (pastConn.Count == 0 || pastConn.Last().EndDateTime <= conn.StartDateTime) {
                        newPastConn = pastConn.Select(item => (Connection)item.Clone()).ToList();
                        newPastConn.Add(conn);

                        if (conn.EndPoint == endPoint) {
                            indirectConn.Add(newPastConn);
                        }
                        else {
                            List<Connection> newAvailableConn = availableConn.Select(item => (Connection)item.Clone()).ToList();
                            FindAll(newAvailableConn, newPastConn, conn.EndPoint, endPoint);
                        }
                    }
                }
            }
        }

        private void TimeFilter(DateTime startDateTime, DateTime endDateTime) {
            indirectConn = indirectConn.FindAll(l => l.First().StartDateTime >= startDateTime && l.Last().EndDateTime <= endDateTime);
        }


        public List<Connection> GetConnections() {
            if(connections.Count == 0 ) {
                throw new FaultException<NoConnectionsException>(new NoConnectionsException());
            }
            return connections;
        }

        public List<List<Connection>> FindConnections(string startPoint, string endPoint) {
            if (ValidateLocation(startPoint) && ValidateLocation(endPoint)) {
                if(!IsAvailable(startPoint)) {
                    throw new FaultException<LocationNotFoundException>(new LocationNotFoundException(startPoint));
                }
                if (!IsAvailable(endPoint)) {
                    throw new FaultException<LocationNotFoundException>(new LocationNotFoundException(endPoint));
                }
                indirectConn = new List<List<Connection>>();
                FindAll(connections, new List<Connection>(), startPoint, endPoint);
                if(indirectConn.Count > 0) {
                    //indirectConn = indirectConn.Sort((l1, l2) => l1.Last().EndDateTime.CompareTo(l2.Last().EndDateTime));
                    return indirectConn;
                }
                else {
                    throw new FaultException<ConnectionDoesNotExistException>(new ConnectionDoesNotExistException(startPoint, endPoint));
                }
            } else {
                throw new FaultException<InvalidArgumentException>(new InvalidArgumentException());
            }
        }

        public List<List<Connection>> FindConnectionsInTime(string startPoint, DateTime startDateTime, string endPoint, DateTime endDateTime) {
            if (ValidateLocation(startPoint) && ValidateLocation(endPoint) && ValidateDate(startDateTime, endDateTime)) {
                if (!IsAvailable(startPoint)) {
                    throw new FaultException<LocationNotFoundException>(new LocationNotFoundException(startPoint));
                }
                if (!IsAvailable(endPoint)) {
                    throw new FaultException<LocationNotFoundException>(new LocationNotFoundException(endPoint));
                }
                indirectConn = new List<List<Connection>>();
                FindAll(connections, new List<Connection>(), startPoint, endPoint);
                TimeFilter(startDateTime, endDateTime);
                if (indirectConn.Count > 0) {
                    return indirectConn;
                }
                else {
                    throw new FaultException<ConnectionDoesNotExistException>(new ConnectionDoesNotExistException(startPoint, endPoint));
                }
            }
            else {
                throw new FaultException<InvalidArgumentException>(new InvalidArgumentException());
            }
        }


    }
}
