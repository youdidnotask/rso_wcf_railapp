﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RailApp {

    [ServiceContract]
    public interface IRailService {

        [OperationContract]
        [FaultContract(typeof(NoConnectionsException))]
        List<Connection> GetConnections();

        [OperationContract]
        [FaultContract(typeof(InvalidArgumentException))]
        [FaultContract(typeof(LocationNotFoundException))]
        [FaultContract(typeof(ConnectionDoesNotExistException))]
        List<List<Connection>> FindConnections(string startPoint, string endPoint);

        [OperationContract]
        [FaultContract(typeof(InvalidArgumentException))]
        [FaultContract(typeof(LocationNotFoundException))]
        [FaultContract(typeof(ConnectionDoesNotExistException))]
        List<List<Connection>> FindConnectionsInTime(string startPoint, DateTime startDateTime, string endPoint, DateTime endDateTime);

    }

    [DataContract]
    public class Connection : ICloneable {
        private string _StartPoint;
        private string _EndPoint;
        private DateTime _StartDateTime;
        private DateTime _EndDateTime;
        private bool _Visited;

        public Connection(string startPoint, DateTime startDateTime, string endPoint, DateTime endDateTime, bool visited) {
            _StartPoint = startPoint;
            _EndPoint = endPoint;
            _StartDateTime = startDateTime;
            _EndDateTime = endDateTime;
            _Visited = visited;
        }

        [DataMember]
        public string StartPoint {
            get { return _StartPoint; }
            set { _StartPoint = value; }
        }

        [DataMember]
        public string EndPoint {
            get { return _EndPoint; }
            set { _EndPoint = value; }
        }

        [DataMember]
        public DateTime StartDateTime {
            get { return _StartDateTime; }
            set { _StartDateTime = value; }
        }

        [DataMember]
        public DateTime EndDateTime {
            get { return _EndDateTime; }
            set { _EndDateTime = value; }
        }

        public bool Visited {
            get { return _Visited; }
            set { _Visited = value; }
        }

        public object Clone() {
            return new Connection(_StartPoint, _StartDateTime, _EndPoint, _EndDateTime, _Visited);
        }
    }

    [DataContract]
    public class LocationNotFoundException {
        private string _Message;

        public LocationNotFoundException(string location) {
            _Message = String.Format("Location {0} not found.", location);
        }

        public LocationNotFoundException(string startLocation, string endLocation) {
            _Message = String.Format("Location {0} and {1} not found", startLocation, endLocation);
        }

        [DataMember]
        public string Message {
            get { return _Message; }
            set { _Message = value; }
        }
    }

    [DataContract]
    public class NoConnectionsException {
        private string _Message;

        public NoConnectionsException() {
            _Message = String.Format("No connections are available.");
        }

        [DataMember]
        public string Message {
            get { return _Message; }
            set { _Message = value; }
        }
    }

    [DataContract]
    public class ConnectionDoesNotExistException {
        private string _Message;

        public ConnectionDoesNotExistException(string startLocation, string endLocation) {
            _Message = String.Format("Connection between {0} and {1} does not exist.", startLocation, endLocation);
        }

        [DataMember]
        public string Message {
            get { return _Message; }
            set { _Message = value; }
        }
    }

    [DataContract]
    public class InvalidArgumentException {
        private string _Message;

        //public InvalidArgumentException(string argument) {
        //    _Message = String.Format("Argument {0} has invalid format.", argument);
        //}

        public InvalidArgumentException() {
            _Message = String.Format("Invalid format.");
        }

        [DataMember]
        public string Message {
            get { return _Message; }
            set { _Message = value; }
        }
    }
}
